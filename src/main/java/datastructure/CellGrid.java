package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];

        for(int r = 0; r < this.rows; r++){ 
            for(int c = 0; c < this.columns; c++){
                this.grid[r][c] = initialState;
            }}
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.rows, this.columns, null);
        for(int r = 0; r < this.rows; r++){ 
            for(int c = 0; c < this.columns; c++){
                gridCopy.set(r, c, this.get(r, c));
            }}


            return gridCopy;
        }
    }
